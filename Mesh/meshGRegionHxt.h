// Gmsh - Copyright (C) 1997-2018 C. Geuzaine, J.-F. Remacle
//
// See the LICENSE.txt file for license information. Please report all
// bugs and problems to the public mailing list <gmsh@onelab.info>.

#ifndef _MESH_GREGION_HXT_
#define _MESH_GREGION_HXT_

#include <vector>

class GRegion;

int meshGRegionHxt(std::vector<GRegion *> &regions);

#endif
