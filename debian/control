Source: gmsh
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Anton Gladky <gladk@debian.org>,
           Christophe Trophime <christophe.trophime@lncmi.cnrs.fr>,
           Kurt Kremitzki <kurt@kwk.systems>
Section: math
Priority: optional
Build-Depends: cmake,
               debhelper (>= 11~),
               dh-exec,
               dh-python,
               freeglut3-dev,
               gfortran,
               libann-dev,
               libblas-dev,
               libcgns-dev,
               libfltk1.3-dev,
               libgl1-mesa-dev,
               libgl2ps-dev,
               libglu1-mesa-dev,
               libgmm++-dev,
               libgmp-dev,
               libjpeg-dev,
               liblapack-dev,
               libmed-dev,
               libmedc-dev,
               libocct-modeling-data-dev,
               libocct-ocaf-dev,
               libocct-visualization-dev,
               libpng-dev,
               libtet1.5-dev,
               mpi-default-bin,
               mpi-default-dev,
               occt-draw,
               python3-dev,
               swig,
               texinfo,
               texlive,
               zlib1g-dev
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/science-team/gmsh
Vcs-Git: https://salsa.debian.org/science-team/gmsh.git
Homepage: http://gmsh.info

Package: gmsh
Architecture: any
Depends: mpi-default-bin,
         ${misc:Depends},
         ${shlibs:Depends},
         ${python3:Depends}
Recommends: gmsh-doc
Description: Three-dimensional finite element mesh generator
 Gmsh is a 3D finite element grid generator with a build-in CAD engine
 and post-processor. Its design goal is to provide a fast, light and
 user-friendly meshing tool with parametric input and advanced
 visualization capabilities. Gmsh is built around four modules: geometry,
 mesh, solver and post-processing. The specification of any input to
 these modules is done either interactively using the graphical user
 interface or in ASCII text files using Gmsh's own scripting language.
 .
 See Gmsh's reference manual for a more thorough overview of Gmsh's
 capabilities.

Package: gmsh-doc
Architecture: all
Section: doc
Depends: mpi-default-bin,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Three-dimensional finite element mesh generator documentation
 Gmsh is a 3D finite element grid generator with a build-in CAD engine
 and post-processor. Its design goal is to provide a fast, light and
 user-friendly meshing tool with parametric input and advanced
 visualization capabilities. Gmsh is built around four modules: geometry,
 mesh, solver and post-processing. The specification of any input to
 these modules is done either interactively using the graphical user
 interface or in ASCII text files using Gmsh's own scripting language.
 .
 See Gmsh's reference manual for a more thorough overview of Gmsh's
 capabilities.
 .
 The package contains documentation and examples.

Package: libgmsh-dev
Architecture: any
Section: libdevel
Depends: libgmsh4.0 (= ${binary:Version}),
         ${misc:Depends}
Recommends: gmsh
Description: Three-dimensional finite element mesh generator development files
 Gmsh is a 3D finite element grid generator with a build-in CAD engine
 and post-processor. Its design goal is to provide a fast, light and
 user-friendly meshing tool with parametric input and advanced
 visualization capabilities. Gmsh is built around four modules: geometry,
 mesh, solver and post-processing. The specification of any input to
 these modules is done either interactively using the graphical user
 interface or in ASCII text files using Gmsh's own scripting language.
 .
 See Gmsh's reference manual for a more thorough overview of Gmsh's
 capabilities.
 .
 The package contains development files.

Package: libgmsh4.0
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Recommends: gmsh
Pre-Depends: ${misc:Pre-Depends}
Description: Three-dimensional finite element mesh generator shared library
 Gmsh is a 3D finite element grid generator with a build-in CAD engine
 and post-processor. Its design goal is to provide a fast, light and
 user-friendly meshing tool with parametric input and advanced
 visualization capabilities. Gmsh is built around four modules: geometry,
 mesh, solver and post-processing. The specification of any input to
 these modules is done either interactively using the graphical user
 interface or in ASCII text files using Gmsh's own scripting language.
 .
 See Gmsh's reference manual for a more thorough overview of Gmsh's
 capabilities.
 .
 The package contains a shared library.

Package: python3-gmsh
Architecture: any
Section: python
Depends: libgmsh4.0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         ${python3:Depends}
Recommends: gmsh
Description: Three-dimensional finite element mesh generator Python 3 wrapper
 Gmsh is a 3D finite element grid generator with a build-in CAD engine
 and post-processor. Its design goal is to provide a fast, light and
 user-friendly meshing tool with parametric input and advanced
 visualization capabilities. Gmsh is built around four modules: geometry,
 mesh, solver and post-processing. The specification of any input to
 these modules is done either interactively using the graphical user
 interface or in ASCII text files using Gmsh's own scripting language.
 .
 See Gmsh's reference manual for a more thorough overview of Gmsh's
 capabilities.
 .
 This package contains the Python 3 wrapper for gmsh
